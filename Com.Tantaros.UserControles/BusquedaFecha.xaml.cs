﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Com.Tantaros.UserControles
{
    /// <summary>
    /// Interaction logic for BusquedaFecha.xaml
    /// </summary>
    public partial class BusquedaFecha : UserControl
    {
        public event EventHandler UserControlButtonClicked;
        public BusquedaFecha()
        {
            InitializeComponent();
            this.DataContext = this;
        }

        public string EtiquetaBoton { get; set; }

        public void Button_Click(object sender, RoutedEventArgs e)
        {
            if(dtInicio.SelectedDate > dtFin.SelectedDate)
            {
                MessageBox.Show("Fecha Inicio Mayor que Fecha Fin");
            }
            else
            {
                if (UserControlButtonClicked != null)
                {
                    UserControlButtonClicked(this, EventArgs.Empty);
                }
            }
        }
    }
}
