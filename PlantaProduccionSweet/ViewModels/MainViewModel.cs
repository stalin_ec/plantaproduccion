﻿using GalaSoft.MvvmLight.Command;
using PlantaProduccionSweet.Controllers;
using PlantaProduccionSweet.lib;
using PlantaProduccionSweet.Model;
using PlantaProduccionSweet.modelos;
using PlantaProduccionSweet.Pages;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace PlantaProduccionSweet.ViewModels
{
    public class MainViewModel : INotifyPropertyChanged
    {
        #region Atributos
        private DateTime startDate = DateTime.Now;
        private DateTime endtDate = DateTime.Now;
        private string message;
        private ObservableCollection<INCIDENCIAS> incidencias;
        private ObservableCollection<IncidenciaModel> incidenciasModelos;
        private IncidenciasController incidenciasController = new IncidenciasController();
        #endregion

        #region Propiedades
        public DateTime StartDate
        {
            get
            {
                return startDate;
            }
            set
            {
                if (startDate != value)
                {
                    startDate = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("StartDate"));
                }
            }
        }

        public DateTime EndtDate
        {
            get
            {
                return endtDate;
            }
            set
            {
                if (endtDate != value)
                {
                    endtDate = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("EndtDate"));
                }
            }
        }

        public string Message
        {
            get
            {
                return message;
            }
            set
            {
                if (message != value)
                {
                    message = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Message"));
                }
            }
        }
        #endregion
        public ObservableCollection<INCIDENCIAS> Incidencias 
        {
            get
            {
                return incidencias;
            }
            set
            {
                if(incidencias != value)
                {
                    incidencias = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Incidencias"));
                }
            }
        } 

        public ObservableCollection<IncidenciaModel> IncidenciasModelos
        { 
            get
            {
                return incidenciasModelos;
            }
            set
            {
                if (incidenciasModelos != value)
                {
                    incidenciasModelos = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IncidenciasModelos"));
                }
            }
        }


        #region Eventos
        public event PropertyChangedEventHandler PropertyChanged; 
        #endregion

        #region Comandos
        public ICommand CargaPedidosCommand { get { return new RelayCommand(CargaPedidos); } }
        public ICommand ValidarPedidosCommand { get { return new RelayCommand(ValidaPedidos); } }

        private void ValidaPedidos()
        {
            ValidarPedidos modal = new ValidarPedidos();
            modal.ShowDialog();
        }

        private void CargaPedidos()
        {
            string Error = "";
            var c = new List<IncidenciaModel>();
            Fecha.ValidaFecha(startDate, endtDate, ref Error);
            if (Error != "")
            {
                Message = Error;
            }
            else
            {
                Incidencias = incidenciasController.GetIncidencias(startDate, endtDate, ref Error);
                Message = Error;
                IncidenciasModelos = incidenciasController.GetIncidenciaModel(startDate, endtDate, ref Error);
            }
        }
        #endregion
    }
}
