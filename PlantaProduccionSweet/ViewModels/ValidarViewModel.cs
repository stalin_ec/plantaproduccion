﻿using GalaSoft.MvvmLight.Command;
using PlantaProduccionSweet.Controllers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using PlantaProduccionSweet.Matriz;
using System.Data;
using System.Windows;
using PlantaProduccionSweet.Model.Matriz;
using System.Windows.Controls;
using PlantaProduccionSweet.lib;
using System.Collections.ObjectModel;
using PlantaProduccionSweet.Pages;
using System.Windows.Forms;
using System.Configuration;

namespace PlantaProduccionSweet.ViewModels
{
    public class ClsTipo
    {
        private int id;
        private string name;
        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public override string ToString()
        {
            return String.Format(Name);
        }
    }

    public class Pedidos
    {
        public int CodCliente { get; set; }
        public string NumSerie { get; set; }
        public int NumPedido { get; set; }
    }
    public class ValidarViewModel : INotifyPropertyChanged
    {
        #region Atributos
        private PedVentaLinController pedVenta = new PedVentaLinController();
        private DataTable pedidos = new DataTable();
        private DataTable pedidosGrupos = new DataTable();
        private DataColumn selectColumn;
        private DataRow selectRow;
        private DataView pedidosView;
        private DataView pedidosViewGrupos;
        private DateTime? pedidoDate;
        private DateTime? entregaDate;
        private string mensaje;
        private bool isValida;
        private ObservableCollection<ClsTipo> tipos = new ObservableCollection<ClsTipo>();
        private ClsTipo tipo = new ClsTipo();
        private MatrizSweet matriz = new MatrizSweet();
        private List<ArticuloPedidoCantidad> articuloPedidoCantidad = new List<ArticuloPedidoCantidad>();
        private List<ArticuloPedidoCantidad> articuloPedidoCantidad_bck = new List<ArticuloPedidoCantidad>();
        private string SerieGuia = ConfigurationManager.AppSettings["SerieGuia"];
        #endregion

        #region Eventos
        public event PropertyChangedEventHandler PropertyChanged; 
        #endregion

        #region Comandos
        public ICommand GuardarPedidosCommand { get { return new RelayCommand(GuardarPedidos); } }

        public ICommand GenerarPedidosCommand { get { return new RelayCommand(GenerarPedidos); } }

        
        public ICommand ImprimirPedidosCommand { get { return new RelayCommand(ImprimirPedidos); } }
        public ICommand CargaPedidosCommand { get { return new RelayCommand(CargaPedidos); } }

        private void CargaPedidos()
        {
            string Error = "";
            Fecha.ValidaFechasNull(PedidoDate.GetValueOrDefault(), EntregaDate.GetValueOrDefault(), ref Error);
            if (Error != "")
            {
                System.Windows.MessageBox.Show(Error.ToString());
                Pedidos.Clear();
                
            }
            else
            {
                articuloPedidoCantidad_bck = pedVenta.GetArticuloPedidoCantidades(PedidoDate.GetValueOrDefault(), EntregaDate.GetValueOrDefault(), ref Error);
                if(articuloPedidoCantidad_bck.Count > 0)
                {
                    IsValida = true;
                }
                else
                {
                    IsValida = false;
                }
                var a = articuloPedidoCantidad_bck.GroupBy(x => x.Tipo).Select(x=>x.Key).ToList();

                articuloPedidoCantidad_bck = articuloPedidoCantidad_bck.Where(x => (x.TODORECIBIDO == "T" && x.SerieAlbaran != "") || (x.TODORECIBIDO == "F" && x.SerieAlbaran == "")).ToList();

                tipos.Clear();

                ClsTipo d = new ClsTipo();
                d.Id = 0;
                d.Name = "TODOS";
                tipos.Add(d);
                foreach (var b in a)
                {
                    ClsTipo c = new ClsTipo();
                    c.Id = b.GetValueOrDefault();
                    c.Name = b.GetValueOrDefault().ToString();
                    tipos.Add(c);
                }
                articuloPedidoCantidad = articuloPedidoCantidad_bck;

                Pedidos = matriz.CreateMatrixDataTable(articuloPedidoCantidad);
                PedidosGrupos = matriz.CreateMatrixDataTableGrupos(articuloPedidoCantidad);
                
                PedidosView = Pedidos.DefaultView;
                PedidosViewGrupos = PedidosGrupos.DefaultView;

                //Mensaje = " Se recuperaron {0} " + Pedidos.Columns.Count;
            }
        }

        private void ActualizaPedidos()
        {
            try
            {
                string Error = "";
                Fecha.ValidaFechasNull(PedidoDate.GetValueOrDefault(), EntregaDate.GetValueOrDefault(), ref Error);
                if (Error != "")
                {
                    System.Windows.MessageBox.Show(Error.ToString());
                    Pedidos.Clear();

                }
                else
                {
                    articuloPedidoCantidad_bck = pedVenta.GetArticuloPedidoCantidades(PedidoDate.GetValueOrDefault(), EntregaDate.GetValueOrDefault(), ref Error);
                    if (articuloPedidoCantidad_bck.Count > 0)
                    {
                        IsValida = true;
                    }
                    else
                    {
                        IsValida = false;
                    }
                    var a = articuloPedidoCantidad_bck.GroupBy(x => x.Tipo).Select(x => x.Key).ToList();

                    tipos.Clear();

                    ClsTipo d = new ClsTipo();
                    d.Id = 0;
                    d.Name = "TODOS";
                    tipos.Add(d);
                    foreach (var b in a)
                    {
                        ClsTipo c = new ClsTipo();
                        c.Id = b.GetValueOrDefault();
                        c.Name = b.GetValueOrDefault().ToString();
                        tipos.Add(c);
                    }
                    articuloPedidoCantidad = articuloPedidoCantidad_bck;

                    //Mensaje = " Se recuperaron {0} " + Pedidos.Columns.Count;
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Error al Mostrar Datos");
            }
            
        }

        private void GuardarPedidos()
        {
            CargaPedidos();
            Reportes reporte = new Reportes(articuloPedidoCantidad);
            //reporte.ShowDialog();

        }
        private void ImprimirPedidos()
        {
            CargaPedidos();
            var a = articuloPedidoCantidad.Select(x => x.NumAlbaran).Distinct().ToArray();
            Reportes reporte = new Reportes(SerieGuia, a);
            //reporte.ShowDialog();
        }

        private void GenerarPedidos()
        {
            var d = articuloPedidoCantidad.Select(x => x).Distinct().ToList();
            var i = articuloPedidoCantidad.GroupBy(o => new { o.NumPedido }).Select(x => x.FirstOrDefault());
            if (articuloPedidoCantidad.Where(x => x.TODORECIBIDO == "F").Any())
            {
                foreach (var a in i)
                {
                    if (a.TODORECIBIDO == "F")
                    {
                        var b = pedVenta.GenerarGuia(a.CodCliente, a.NumSerie, a.NumPedido, SerieGuia, 1);
                    }
                }
                
            }
            else
            {
                System.Windows.MessageBox.Show("Todos los Pedidos de este Grupo ya fueron Generados");
            }
            
        }


        #endregion

        #region Propiedades

        public Nullable<DateTime> EntregaDate
        {
            get
            {
                return entregaDate;
            }
            set
            {
                if (entregaDate != value)
                {
                    entregaDate = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("EntregaDate"));
                }
            }
        }

        public bool IsValida
        {
            get
            {
                return isValida;
            }
            set
            {
                if(isValida != value)
                {
                    isValida = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsValida"));
                }
            }
        }

        public Nullable<DateTime> PedidoDate
        {
            get
            {
                return pedidoDate;
            }
            set
            {
                if (pedidoDate != value)
                {
                    pedidoDate = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("PedidoDate"));
                }
            }
        }

        public ClsTipo Tipo
        {
            get
            {
                return tipo;
            }
            set
            {
                if(tipo != value)
                {
                    tipo = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Tipo"));
                    System.Windows.MessageBox.Show(Tipo.ToString());
                    var a = Tipo.Id;
                    if(a != 0)
                    {
                        articuloPedidoCantidad = articuloPedidoCantidad_bck.Where(x => x.Tipo == a).Select(x => x).ToList();
                    }
                    else
                    {
                        articuloPedidoCantidad = articuloPedidoCantidad_bck;
                    }
                    Pedidos = matriz.CreateMatrixDataTable(articuloPedidoCantidad);
                    PedidosView = Pedidos.DefaultView;
                }
            }
        }

        public ObservableCollection<ClsTipo> Tipos
        {
            get
            {
                return tipos;
            }
            set
            {
                if(tipos != value)
                {
                    tipos = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Tipos"));
                }
            }
        }

        public string Mensaje
        {
            get
            {
                return mensaje;
            }
            set
            {
                if(mensaje != null)
                {
                    mensaje = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Mensaje"));
                }
            }
        }
        public DataRow SelectRow
        {
            get
            {
                return selectRow;
            }
            set
            {
                if (selectRow != null)
                {
                    selectRow = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("SelectRow"));
                }
            }
        }
        public DataColumn SelectColumn
        {
            get
            {
                return selectColumn;
            }
            set
            {
                if (selectColumn != null)
                {
                    selectColumn = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("SelectColumn"));
                }
            }
        }
        public DataTable Pedidos
        {
            get
            {
                return pedidos;
            }
            set
            {
                if (pedidos != value)
                {
                    pedidos = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Pedidos"));
                    pedidos.Columns[0].ReadOnly = true;
                    pedidos.Columns[1].ReadOnly = true;
                    pedidos.ColumnChanged += Pedidos_ColumnChanged;                   
                }
            }
        }
        public DataTable PedidosGrupos
        {
            get
            {
                return pedidosGrupos;
            }
            set
            {
                if (pedidosGrupos != value)
                {
                    pedidosGrupos = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("PedidosGrupos"));
                    pedidosGrupos.Columns[0].ReadOnly = true;
                    pedidosGrupos.Columns[1].ReadOnly = true;
                }
            }
        }


        private void Pedidos_ColumnChanging(object sender, DataColumnChangeEventArgs e)
        {
            var a = sender;
            var b = e;
            var cabecera = e.Column.Caption;
            try
            {
                var original1 = e.Row[e.ProposedValue.ToString(), DataRowVersion.Original];
                System.Windows.MessageBox.Show(original1.ToString());
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.ToString());
            }
        }

        public DataView PedidosView
        {
            get
            {
                return pedidos.DefaultView;
            }
            set
            {
                if (pedidos.DefaultView != null)
                {
                    pedidosView = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("PedidosView"));
                }
            }
        }

        public DataView PedidosViewGrupos
        {
            get
            {
                return pedidosGrupos.DefaultView;
            }
            set
            {
                if (pedidosGrupos.DefaultView != null)
                {
                    pedidosViewGrupos = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("PedidosViewGrupos"));
                }
            }
        }
        #endregion

        private void Pedidos_ColumnChanged(object sender, DataColumnChangeEventArgs e)
        {
            try
            {
                PedVentaLinController pedVentaLinController = new PedVentaLinController();
                var d = SelectColumn;
                var cabecera = e.Column.ColumnName;
                var r = e.Row;
                var col = e.Column;
                var asd = r.HasVersion(DataRowVersion.Original);
                double cantidad;
                bool isDouble = Double.TryParse(e.ProposedValue.ToString(), out cantidad);
                if (!isDouble)
                {
                    e.Row.CancelEdit();
                    System.Windows.MessageBox.Show("Solo se permiten numeros");
                }
                else
                {
                    var fila = e.Row.ItemArray[0].ToString();
                    var serie = articuloPedidoCantidad.Where(x => x.Cabecera == cabecera).Select(x => x.NumSerie).First();
                    var pedido = articuloPedidoCantidad.Where(x => x.Cabecera == cabecera).Select(x => x.NumPedido).First();
                    var codarticulo = articuloPedidoCantidad.Where(x => x.Referencia == fila).Select(x => x.CodArticulo).First();
                    var valor = double.Parse(e.ProposedValue.ToString());
                    var c = pedVentaLinController.ActualizaPedidoLinea(codarticulo.ToString(), serie.ToString(), pedido.ToString(), (float)cantidad);
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Error al grabar nuevos Datos");
            }
            
            //CargaPedidos();
        }

        private void Pedidos_RowChanged(object sender, DataRowChangeEventArgs e)
        {
            var evento = e;
            var nombre = e.Row["2", DataRowVersion.Original];
            var row = (sender) as DataRowView;
        } 
    }
}
 