﻿using MahApps.Metro.Controls;
using PlantaProduccionSweet.Controllers;
using PlantaProduccionSweet.lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PlantaProduccionSweet
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        private static Mutex mutex;


        private static bool PrimeraInstancia
        {
            get
            {
                string fullName = Assembly.GetEntryAssembly().FullName;
                bool result;
                MainWindow.mutex = new Mutex(true, fullName, out result);
                return result;
            }
        }
        public MainWindow()
        {
            InitializeComponent();
            if (!MainWindow.PrimeraInstancia)
            {
                MessageBox.Show("Ya se encuentra una instancia en Ejecución");
                Environment.Exit(0);
            }
        }
    }
}
