﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PlantaProduccionSweet.Pages
{
    /// <summary>
    /// Interaction logic for ValidarPedidos.xaml
    /// </summary>
    public partial class ValidarPedidos : MetroWindow
    {
        public ValidarPedidos()
        {
            InitializeComponent();
        }

        private void dtgPedidos_Loaded(object sender, RoutedEventArgs e)
        {
            //dtgPedidos.Columns[1].Width = 100;
            //dtgPedidos.Columns[2].Width = 200;
        }

        private void dtgPedidos_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            dtgPedidos.Columns[1].Width = 200;
            //dtgPedidos.Columns[1].
            //dtgPedidos.Columns[2].Width = 200;
        }
    }
}
