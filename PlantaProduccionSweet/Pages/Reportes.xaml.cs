﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Reporting.WinForms;
using PlantaProduccionSweet.Controllers;
using PlantaProduccionSweet.Model.Matriz;

namespace PlantaProduccionSweet.Pages
{
    /// <summary>
    /// Interaction logic for Reportes.xaml
    /// </summary>
    public partial class Reportes : Window
    {
        public GuiasController guias = new GuiasController();
        public Reportes()
        {
            InitializeComponent();
            viewerInstance.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", guias.GetGuiaRemisions()));
            viewerInstance.LocalReport.ReportPath = "..//..//Reportes/Guias.rdlc";
            viewerInstance.RefreshReport();
        }
        public Reportes(string SerieAlbaran, int?[] NumAlbaran)
        {
            InitializeComponent();
            foreach(var a in NumAlbaran)
            {
                LocalReport localReport = new LocalReport();
                localReport.DataSources.Add(new ReportDataSource("DataSet1", guias.GetGuiaRemisions(SerieAlbaran, a)));
                localReport.ReportPath = "..//..//Reportes/Guias_bck.rdlc";
                PlantaProduccionSweet.Controls.PrintRDLCReport.PrintToPrinter(localReport);
            }
            /*
            viewerInstance.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", guias.GetGuiaRemisions(SerieAlbaran, NumAlbaran)));
            viewerInstance.LocalReport.ReportPath = "..//..//Reportes/Guias_bck.rdlc";
            viewerInstance.RefreshReport();
            */
        }

        public Reportes(List<ArticuloPedidoCantidad> a)
        {
            InitializeComponent();
            // viewerInstance.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", a));
            // viewerInstance.LocalReport.ReportPath = "..//..//Reportes/PedidosGuia.rdlc";

            var i = a.GroupBy(o => new { o.NumPedido }).Select(x=>x.FirstOrDefault());

            foreach (var b in i)
            {
                List<ArticuloPedidoCantidad> c = new List<ArticuloPedidoCantidad>();
                var e = a.Where(x => x.NumPedido == b.NumPedido);
                //c.Add(e);
                LocalReport localReport = new LocalReport();
                localReport.DataSources.Add(new ReportDataSource("DataSet1", e));
                localReport.ReportPath = "..//..//Reportes/PedidosGuia.rdlc";
                PlantaProduccionSweet.Controls.PrintRDLCReport.PrintToPrinter(localReport);
                //viewerInstance.RefreshReport();
                // viewerInstance.ShowPrintButton = false;
                c.Clear();
            }
        }
    }
}
