﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PlantaProduccionSweet.Controls
{
    public class MatrixColumn
    {
        public string Name { get; set; }
        public string StringFormat { get; set; }

        public override string ToString()
        {
            return this.Name;
        }
    }
}