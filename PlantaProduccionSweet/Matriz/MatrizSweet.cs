﻿using PlantaProduccionSweet.Controls;
using PlantaProduccionSweet.Model.Matriz;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlantaProduccionSweet.Matriz
{
    public class MatrizSweet
    {
        public DataMatrix CreateMatrix(IEnumerable<ArticuloPedidoCantidad> orders)
        {
            var cliente = orders.Select(x => x.NombreCliente).Distinct().ToList();
            var CodArticulos = orders.Select(x => x.CodArticulo).Distinct().ToList();
            DataMatrix result = new DataMatrix { Columns = new List<MatrixColumn>(), Rows = new List<object[]>() };
            result.Columns.Add(new MatrixColumn() { Name = "Articulos" });
            for(int i = 0; i < cliente.Count(); i++)
            {
                result.Columns.Add(new MatrixColumn() 
                {
                    Name = cliente[i]
                });
            }
            for (int i = 0; i < CodArticulos.Count(); i++)
            {
                object[] row = new object[cliente.Count()+1];
                row[0] = (from o in orders where o.CodArticulo == CodArticulos[i] select o.Descripcion).First();
                for (int j = 1; j <= cliente.Count(); j++)
                {
                    var count = (from o in orders where o.CodArticulo == CodArticulos[i] && o.NombreCliente == cliente[j-1] select o.Cantidad).Sum();
                    row[j] = count;
                }
                result.Rows.Add(row);
            }
            return result;
        }

        public DataTable CreateMatrixDataTable(IEnumerable<ArticuloPedidoCantidad> orders)
        {
            /*
            DataTable dataTable = new DataTable("PEDIDOS");
            int cont = 0;
            var cliente = orders.Select(x => x.NombreCliente).Distinct().ToList();
            var CodArticulos = orders.Select(x => x.CodArticulo).Distinct().ToList();
            var Referencia = orders.Select(x => x.Referencia).Distinct().ToList();
            var Orden = orders.Select(x => x.Orden).Distinct().ToList();
            dataTable.Columns.Add(new DataColumn("Referencia"));
            dataTable.Columns.Add(new DataColumn("Articulo Descripcion"));
            dataTable.Columns["Referencia"].Unique = true;
            dataTable.PrimaryKey = new DataColumn[] { dataTable.Columns["Referencia"] };
            for (int i = 0; i < cliente.Count(); i++)
            {
                dataTable.Columns.Add(new DataColumn(cliente[i]+"\r\n"+ Orden[i]));
            }
            dataTable.Columns.Add(new DataColumn("Total"));
            dataTable.Columns.Add(new DataColumn("Stock"));
            dataTable.Columns.Add(new DataColumn("Diferencia"));
            for (int i = 0; i < CodArticulos.Count(); i++)
            {
                DataRow r = dataTable.NewRow();
                r[0] = Referencia[i];
                r[1] = (from o in orders where o.CodArticulo == CodArticulos[i] select o.Descripcion).First();
                for (int j = 1; j <= cliente.Count(); j++)
                {
                    var count = (from o in orders where o.CodArticulo == CodArticulos[i] && o.NombreCliente == cliente[j - 1] select o.Cantidad).Sum();
                    r[j+1] = count;
                    cont = j + 1;
                }
                cont = cont + 1;
                var total = (from o in orders where o.CodArticulo == CodArticulos[i] select o.Cantidad).Sum();
                r[cont] = String.Format("{0:0.##}", total);
                cont = cont + 1;
                var stock = (from o in orders where o.CodArticulo == CodArticulos[i] select o.Stock).First();
                r[cont] = String.Format("{0:0.##}", stock);
                cont = cont + 1;
                r[cont] = String.Format("{0:0.##}", stock - total);
                dataTable.Rows.Add(r);
            }
            return dataTable;*/
            DataTable dataTable = new DataTable("PEDIDOS");
            int cont = 0;
            var cliente = orders.Select(x => x.Alias).Distinct().ToList();
            var CodArticulos = orders.Select(x => x.CodArticulo).Distinct().ToList();
            var Referencia = orders.Select(x => x.Referencia).Distinct().ToList();
            var Orden = orders.Select(x => x.Orden).Distinct().ToList();
            dataTable.Columns.Add(new DataColumn("Referencia"));
            dataTable.Columns.Add(new DataColumn("Articulo Descripcion"));
            dataTable.Columns["Referencia"].Unique = true;
            dataTable.PrimaryKey = new DataColumn[] { dataTable.Columns["Referencia"] };
            for (int i = 0; i < cliente.Count(); i++)
            {
                var c = orders.Where(x => x.Alias == cliente[i]).Select(x=>x.Orden).Distinct().ToList();
                foreach(var d in c)
                {
                    dataTable.Columns.Add(new DataColumn(cliente[i] + "\r\n" + d));
                }               
            }
            dataTable.Columns.Add(new DataColumn("Total"));
            dataTable.Columns.Add(new DataColumn("Stock"));
            dataTable.Columns.Add(new DataColumn("Diferencia"));
            for (int i = 0; i < CodArticulos.Count(); i++)
            {
                DataRow r = dataTable.NewRow();
                r[0] = Referencia[i];
                r[1] = (from o in orders where o.CodArticulo == CodArticulos[i] select o.Descripcion).First();
                for(int k = 1; k <= Orden.Count(); k++)
                {
                    var count = (from o in orders where o.CodArticulo == CodArticulos[i] &&  o.Orden == Orden[k - 1] select o.Cantidad).Sum();
                    r[k + 1] = count;
                    cont = k + 1;
                }
                cont = cont + 1;
                var total = (from o in orders where o.CodArticulo == CodArticulos[i] select o.Cantidad).Sum();
                r[cont] = String.Format("{0:0.##}", total);
                cont = cont + 1;
                var stock = (from o in orders where o.CodArticulo == CodArticulos[i] select o.Stock).First();
                r[cont] = String.Format("{0:0.##}", stock);
                cont = cont + 1;
                r[cont] = String.Format("{0:0.##}", stock - total);
                dataTable.Rows.Add(r);
            }
            return dataTable;
        }
        public DataTable CreateMatrixDataTableGrupos(IEnumerable<ArticuloPedidoCantidad> orders)
        {
            DataTable dataTable = new DataTable("PEDIDOS");
            int cont = 0;
            var tipo = orders.Select(x => x.Tipo).Distinct().ToList();
            var CodArticulos = orders.Select(x => x.CodArticulo).Distinct().ToList();
            var Referencia = orders.Select(x => x.Referencia).Distinct().ToList();
            dataTable.Columns.Add(new DataColumn("Referencia"));
            dataTable.Columns.Add(new DataColumn("Articulo Descripcion"));

            dataTable.Columns["Referencia"].Unique = true;
            // dataTable.Columns["Referencia"].
            dataTable.PrimaryKey = new DataColumn[] { dataTable.Columns["Referencia"] };
            for (int i = 0; i < tipo.Count(); i++)
            {
                dataTable.Columns.Add(new DataColumn(tipo[i].ToString()));
            }
            dataTable.Columns.Add(new DataColumn("Total"));
            dataTable.Columns.Add(new DataColumn("Stock"));
            dataTable.Columns.Add(new DataColumn("Diferencia"));
            for (int i = 0; i < CodArticulos.Count(); i++)
            {
                DataRow r = dataTable.NewRow();
                r[0] = Referencia[i];
                r[1] = (from o in orders where o.CodArticulo == CodArticulos[i] select o.Descripcion).First();
                for (int j = 1; j <= tipo.Count(); j++)
                {
                    var count = (from o in orders where o.CodArticulo == CodArticulos[i] && o.Tipo == tipo[j - 1] select o.Cantidad).Sum();
                    r[j + 1] = count;
                    cont = j + 1;
                }
                cont = cont + 1;
                var total = (from o in orders where o.CodArticulo == CodArticulos[i] select o.Cantidad).Sum();
                r[cont] = String.Format("{0:0.##}", total);
                cont = cont + 1;
                var stock = (from o in orders where o.CodArticulo == CodArticulos[i] select o.Stock).First();
                r[cont] = String.Format("{0:0.##}", stock);
                cont = cont + 1;
                r[cont] = String.Format("{0:0.##}", stock - total);
                dataTable.Rows.Add(r);
            }
            return dataTable;
        }
    }
}
