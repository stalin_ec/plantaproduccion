﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace PlantaProduccionSweet.lib
{
    public static class Fecha
    {
        public static bool ValidaFecha(DateTime inicio, DateTime fin, ref string Error)
        {
            bool resultado = false;
            if(inicio == null)
            {
                Error = "Fecha Inicial esta en Blanco";
                resultado = false;
            } else if (fin == null)
            {
                Error = "Fecha Final esta en Blanco";
                resultado = false;
            } else if (inicio > fin)
            {
                Error = "Fecha Inicio es mayor que Fecha Final";
                resultado = false;
            }
            else
            {
                resultado = true;
            }
            return resultado;
        }
        public static bool ValidaFechasNull(DateTime pedido, DateTime entrega, ref string Error)
        {
            if(pedido == DateTime.MinValue && entrega == DateTime.MinValue)
            {
                Error = "Ambas fechas no pueden estar en BLANCO.";
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
