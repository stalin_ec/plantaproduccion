﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlantaProduccionSweet.lib
{
    public static class DataAccess
    {
        private static string strConexion = ConfigurationManager.ConnectionStrings["Planta"].ToString();

        private static int cmdTimeOut = int.Parse(ConfigurationManager.AppSettings["segsTimeout"].ToString());
        public static DataTable ConsultaDatos(string Consulta, ref String Error)
        {
            DataTable dataTable = new DataTable();
            SqlConnection sqlConnection = new SqlConnection(DataAccess.strConexion);
            SqlCommand sqlCommand = new SqlCommand(Consulta, sqlConnection);
            DataTable result;
            try
            {
                sqlConnection.Open();
                SqlDataReader reader = sqlCommand.ExecuteReader();
                dataTable.Load(reader);
                result = dataTable;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                result = new DataTable();
            }
            return result;
        }

        public static bool EjecutaSQL(string consulta, ref string Error)
        {
            bool result;
            try
            {
                SqlConnection sqlConnection = new SqlConnection(DataAccess.strConexion);
                SqlCommand sqlCommand = new SqlCommand(consulta, sqlConnection);
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
                result = true;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                result = false;
            }
            return result;
        }
    }
}
