﻿using PlantaProduccionSweet.Model.Matriz;
using PlantaProduccionSweet.modelos;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlantaProduccionSweet.Controllers
{
    public class PedVentaLinController
    {
        public enum Opcion
        {
            Pedido = 1,
            Entrega = 2,
            Todos = 3
        };
        public ObservableCollection<PEDVENTALIN> GetPedVentaLin(string NumSerie, int NumPedido, ref string Error)
        {
            ObservableCollection<PEDVENTALIN> resultado = new ObservableCollection<PEDVENTALIN>();
            try
            {
                using(var db = new SCEntities())
                {
                    var a = db.PEDVENTALIN.Where(x => x.NUMSERIE == NumSerie).Where(x => x.NUMPEDIDO == NumPedido).ToList();
                    resultado = new ObservableCollection<PEDVENTALIN>(a);
                    Error = Error = "Se extrajeron : " + resultado.Count() + " Registros. ";
                }
            }
            catch (Exception ex)
            {
                Error = ex.ToString();
                resultado = null;
            }
            return resultado;
        }

        public List<ArticuloPedidoCantidad> GetArticuloPedidoCantidades()
        {
            List<ArticuloPedidoCantidad> resultado = new List<ArticuloPedidoCantidad>();
            // resultado = null;
            try
            {
                using(var db = new SCEntities())
                {
                    var pedidos = from o in db.PEDVENTALIN
                                  where o.NUMSERIE == "ABP" && o.CODARTICULO > -1
                                  join oo in db.PEDVENTACAB on new { o.NUMSERIE, o.NUMPEDIDO } equals new { oo.NUMSERIE, oo.NUMPEDIDO }
                                  join c in db.CLIENTES on oo.CODCLIENTE equals c.CODCLIENTE
                                  group o by new { o.NUMSERIE, o.NUMPEDIDO, c.CODCLIENTE, c.NOMBRECOMERCIAL, o.REFERENCIA, o.CODARTICULO, o.DESCRIPCION }
                                  into p
                                  where p.Key.NUMPEDIDO == 45548 || p.Key.NUMPEDIDO == 45550 || p.Key.NUMPEDIDO == 45555
                                  select new ArticuloPedidoCantidad
                                  {
                                      NumSerie = p.Key.NUMSERIE,
                                      NumPedido = p.Key.NUMPEDIDO,
                                      CodCliente = p.Key.CODCLIENTE,
                                      NombreCliente = p.Key.NOMBRECOMERCIAL,
                                      Referencia = p.Key.REFERENCIA,
                                      CodArticulo = p.Key.CODARTICULO,
                                      Descripcion = p.Key.DESCRIPCION,
                                      Cantidad = p.Sum(x => x.UNID1)
                                  };
                    resultado = pedidos.ToList();
                }
            }
            catch (Exception ex)
            {
                var error = ex.ToString();
                resultado = null;
            }
            return resultado;
        }

        /// <summary>
        /// Retorna una Lista con todos los Pedidos Busca Por Fecha del Pedido y Por Fecha de Entrega o por cualquiera de las dos.
        /// </summary>
        /// <param name="FechaPedido"></param>
        /// <param name="FechaEntrega"></param>
        /// <param name="Error"></param>
        /// <returns>List<ArticuloPedidoCantidad></returns>
        public List<ArticuloPedidoCantidad> GetArticuloPedidoCantidades(DateTime FechaPedido, DateTime FechaEntrega, ref string Error)
        {
            List<ArticuloPedidoCantidad> resultado = new List<ArticuloPedidoCantidad>();

            string SeriePedido = ConfigurationManager.AppSettings["SeriePedido"];
            string BodegaPrincipal = ConfigurationManager.AppSettings["BodegaPrincipal"];

            Opcion opcion;

            if (FechaPedido == DateTime.MinValue && FechaEntrega != null)
            {
                opcion = Opcion.Entrega;
            }
            else if (FechaPedido != null && FechaEntrega == DateTime.MinValue)
            {
                opcion = Opcion.Pedido;
            }
            else if (FechaPedido != null && FechaEntrega != null)
            {
                opcion = Opcion.Todos;
            }
            else
            {
                Error = "Error en datos de Ingreso";
                return resultado;
            }
            try
            {
                using (var db = new SCEntities())
                {                       
                    switch(opcion)
                    {
                        case Opcion.Entrega:
                            resultado = (from o in db.PEDVENTALIN
                                          where o.NUMSERIE == SeriePedido && o.CODARTICULO > -1
                                          join oo in db.PEDVENTACAB on new { o.NUMSERIE, o.NUMPEDIDO} equals new { oo.NUMSERIE, oo.NUMPEDIDO }
                                          join a in db.ARTICULOS on o.CODARTICULO equals a.CODARTICULO
                                          join s in db.STOCKS on new { V = BodegaPrincipal, p1 = o.CODARTICULO } equals new { V = s.CODALMACEN, p1 = (int?)s.CODARTICULO }
                                          join c in db.CLIENTES on oo.CODCLIENTE equals c.CODCLIENTE
                                          group o by new { o.NUMSERIE, o.NUMPEDIDO, oo.SERIEALBARAN, oo.TODORECIBIDO, oo.NUMEROALBARAN, c.CODCLIENTE, c.DIRECCION1, c.NOMBRECLIENTE, c.ALIAS, o.REFERENCIA, o.CODARTICULO, o.DESCRIPCION, a.UNID2C, a.UNIDADMEDIDA, oo.FECHAENTREGA, oo.FECHAPEDIDO, c.TIPO, s.STOCK }
                                    into p
                                          where p.Key.FECHAENTREGA == FechaEntrega
                                          select new ArticuloPedidoCantidad
                                          {
                                              NumSerie = p.Key.NUMSERIE,
                                              NumPedido = p.Key.NUMPEDIDO,
                                              SerieAlbaran = p.Key.SERIEALBARAN,
                                              NumAlbaran = p.Key.NUMEROALBARAN,
                                              TODORECIBIDO = p.Key.TODORECIBIDO,
                                              FechaEntrega = p.Key.FECHAENTREGA,
                                              CodCliente = p.Key.CODCLIENTE,
                                              Direccion = p.Key.DIRECCION1,
                                              NombreCliente = p.Key.NOMBRECLIENTE,
                                              Alias = p.Key.ALIAS,
                                              Referencia = p.Key.REFERENCIA,
                                              CodArticulo = p.Key.CODARTICULO,
                                              Descripcion = p.Key.DESCRIPCION,
                                              Unidad = p.Key.UNID2C,
                                              Medida = p.Key.UNIDADMEDIDA,
                                              Tipo = p.Key.TIPO,
                                              Stock = p.Key.STOCK,
                                              Cantidad = p.Sum(x => x.UNID1)
                                              
                                          }).OrderBy(x => x.NombreCliente).ToList();
                            break;
                        case Opcion.Pedido:
                            resultado = (from o in db.PEDVENTALIN
                                         where o.NUMSERIE == SeriePedido && o.CODARTICULO > -1
                                         join oo in db.PEDVENTACAB on new { o.NUMSERIE, o.NUMPEDIDO } equals new { oo.NUMSERIE, oo.NUMPEDIDO }
                                         join a in db.ARTICULOS on o.CODARTICULO equals a.CODARTICULO
                                         join s in db.STOCKS on new { V = BodegaPrincipal, p1 = o.CODARTICULO } equals new { V = s.CODALMACEN, p1 = (int?)s.CODARTICULO }
                                         join c in db.CLIENTES on oo.CODCLIENTE equals c.CODCLIENTE
                                         group o by new { o.NUMSERIE, o.NUMPEDIDO, oo.SERIEALBARAN, oo.TODORECIBIDO, oo.NUMEROALBARAN, c.CODCLIENTE, c.DIRECCION1, c.NOMBRECLIENTE, c.ALIAS, o.REFERENCIA, o.CODARTICULO, o.DESCRIPCION, a.UNID2C, a.UNIDADMEDIDA, oo.FECHAENTREGA, oo.FECHAPEDIDO, c.TIPO, s.STOCK }
                                    into p
                                          where p.Key.FECHAPEDIDO == FechaPedido
                                          select new ArticuloPedidoCantidad
                                          {
                                              NumSerie = p.Key.NUMSERIE,
                                              NumPedido = p.Key.NUMPEDIDO,
                                              SerieAlbaran = p.Key.SERIEALBARAN,
                                              NumAlbaran = p.Key.NUMEROALBARAN,
                                              TODORECIBIDO = p.Key.TODORECIBIDO,
                                              FechaEntrega = p.Key.FECHAENTREGA,
                                              CodCliente = p.Key.CODCLIENTE,
                                              Direccion = p.Key.DIRECCION1,
                                              NombreCliente = p.Key.NOMBRECLIENTE,
                                              Alias = p.Key.ALIAS,
                                              Referencia = p.Key.REFERENCIA,
                                              CodArticulo = p.Key.CODARTICULO,
                                              Descripcion = p.Key.DESCRIPCION,
                                              Unidad = p.Key.UNID2C,
                                              Medida = p.Key.UNIDADMEDIDA,
                                              Tipo = p.Key.TIPO,
                                              Stock = p.Key.STOCK,
                                              Cantidad = p.Sum(x => x.UNID1)
                                          }).OrderBy(x => x.NombreCliente).ToList();
                            break;
                        case Opcion.Todos:
                            resultado = (from o in db.PEDVENTALIN
                                         where o.NUMSERIE == SeriePedido && o.CODARTICULO > -1
                                         join oo in db.PEDVENTACAB on new { o.NUMSERIE, o.NUMPEDIDO } equals new { oo.NUMSERIE, oo.NUMPEDIDO }
                                         join a in db.ARTICULOS on o.CODARTICULO equals a.CODARTICULO
                                         join s in db.STOCKS on new { V = BodegaPrincipal, p1 = o.CODARTICULO } equals new { V = s.CODALMACEN, p1 = (int?)s.CODARTICULO }
                                         join c in db.CLIENTES on oo.CODCLIENTE equals c.CODCLIENTE
                                         group o by new { o.NUMSERIE, o.NUMPEDIDO, oo.SERIEALBARAN, oo.TODORECIBIDO, oo.NUMEROALBARAN, c.CODCLIENTE, c.DIRECCION1, c.NOMBRECLIENTE, c.ALIAS, o.REFERENCIA, o.CODARTICULO, o.DESCRIPCION, a.UNID2C, a.UNIDADMEDIDA, oo.FECHAENTREGA, oo.FECHAPEDIDO, c.TIPO, s.STOCK }
                                    into p
                                          where p.Key.FECHAENTREGA == FechaEntrega && p.Key.FECHAPEDIDO == FechaPedido
                                          select new ArticuloPedidoCantidad
                                          {
                                              NumSerie = p.Key.NUMSERIE,
                                              NumPedido = p.Key.NUMPEDIDO,
                                              SerieAlbaran = p.Key.SERIEALBARAN,
                                              NumAlbaran = p.Key.NUMEROALBARAN,
                                              TODORECIBIDO = p.Key.TODORECIBIDO,
                                              FechaEntrega = p.Key.FECHAENTREGA,
                                              CodCliente = p.Key.CODCLIENTE,
                                              Direccion = p.Key.DIRECCION1,
                                              NombreCliente = p.Key.NOMBRECLIENTE,
                                              Alias = p.Key.ALIAS,
                                              Referencia = p.Key.REFERENCIA,
                                              CodArticulo = p.Key.CODARTICULO,
                                              Descripcion = p.Key.DESCRIPCION,
                                              Unidad = p.Key.UNID2C,
                                              Medida = p.Key.UNIDADMEDIDA,
                                              Tipo = p.Key.TIPO,
                                              Stock = p.Key.STOCK,
                                              Cantidad = p.Sum(x => x.UNID1)
                                          }).OrderBy(x=>x.NombreCliente).ToList();
                            break;
                        default:
                            resultado = null;
                            break;
                    }                   
                }
            }
            catch (Exception ex)
            {
                var error = ex.ToString();
                resultado = null;
            }
            Error = "Se recuperaron: {0} " + resultado.Count() + " registros.";
            return resultado;
        }

        public bool ActualizaPedidoLinea(string codarticulo, string serie, string numpedido, float unidades)
        {
            bool resultado = false;
            try
            {
                using(var db = new SCEntities())
                {
                    var a = db.ICG_MY_UPDATE_MATRIZ_CGM(codarticulo, serie, numpedido, unidades);
                    resultado = true;
                }
            }
            catch (Exception)
            {
                resultado = false;
            }
            return resultado;
        }

        public string GenerarGuia(int CodCliente, string NumSerie, int NumPedido, string SerieGuia, int Tipo)
        {
            string resultado = string.Empty;
            try
            {
                using( var db = new SCEntities())
                {
                    var a = db.SIS_PEDIDOS_ALBARANES(CodCliente, NumSerie, NumPedido, SerieGuia, Tipo);
                }
                return "Generado : " + NumSerie;
            }
            catch (Exception ex)
            {
                resultado = ex.ToString();
            }
            return resultado;
        }
    }
}
