﻿using PlantaProduccionSweet.Model;
using PlantaProduccionSweet.modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlantaProduccionSweet.Controllers
{
    public class GuiasController
    {
        public List<GuiaRemision> GetGuiaRemisions()
        {
            SCEntities db = null;
            List<GuiaRemision> guiaRemisions = new List<GuiaRemision>();
            try
            {
                using(db = new SCEntities())
                {
                    var guia = new int[] { 14732, 14733 };
                    var resultado = (from a in db.ALBVENTACAB
                                     where a.NUMSERIE == "AAPG" && guia.Contains(a.NUMALBARAN)
                                     from a2 in db.ALBVENTALIN.Where(v => v.NUMSERIE == a.NUMSERIE).Where(v => v.NUMALBARAN == a.NUMALBARAN).DefaultIfEmpty()
                                     from a3 in db.ALBVENTACAMPOSLIBRES.Where(v=> v.NUMSERIE == a.NUMSERIE).Where(v=> v.NUMALBARAN == a.NUMALBARAN).DefaultIfEmpty()
                                     from a4 in db.ALBVENTAFIRMA.Where(v=> v.SERIE == a.NUMSERIE).Where(v=>v.NUMERO == a.NUMALBARAN).DefaultIfEmpty()
                                     from a5 in db.ARTICULOS.Where(v=> v.CODARTICULO == a2.CODARTICULO).DefaultIfEmpty()
                                     from a6 in db.ICG_CONDUCTORES.Where(v=>v.CODCONDUCTOR == a3.CHOFER).DefaultIfEmpty()
                                     from a7 in db.CLIENTES.Where(v=>v.CODCLIENTE == a.CODCLIENTE).DefaultIfEmpty()
                                     select new GuiaRemision 
                                     {
                                         NombreConductor = a6.NOMBRECONDUCTOR,
                                         IdConductor = a6.IDENTIFICACION,
                                         Placa_Alb = a3.PLACA_ALB,
                                         NumSerie = a.NUMSERIE,
                                         NumAlbaran = a.NUMALBARAN,
                                         Fecha = a.FECHA,
                                         Referencia = a2.REFERENCIA,
                                         Cantidad = a2.UNID1,
                                         Descripcion = a2.DESCRIPCION,
                                         Unidad = a5.UNID2C,
                                         Medida = a5.UNIDADMEDIDA,
                                         NombreCliente = a7.NOMBRECLIENTE,
                                         Direccion = a7.DIRECCION1,
                                         ClaveAcceso = a4.CLAVEACCESO
                                     }).ToList();

                    guiaRemisions = resultado;
                }
            }
            catch (Exception ex)
            {
                guiaRemisions = null;
                db?.Dispose();
            }
            return guiaRemisions;
        }

        public List<GuiaRemision> GetGuiaRemisions(string serie, int?[] numeros)
        {
            SCEntities db = null;
            List<GuiaRemision> guiaRemisions = new List<GuiaRemision>();
            try
            {
                using (db = new SCEntities())
                {
                    var resultado = (from a in db.ALBVENTACAB
                                     where a.NUMSERIE == serie && numeros.Contains(a.NUMALBARAN)
                                     from a2 in db.ALBVENTALIN.Where(v => v.NUMSERIE == a.NUMSERIE).Where(v => v.NUMALBARAN == a.NUMALBARAN).DefaultIfEmpty()
                                     from a3 in db.ALBVENTACAMPOSLIBRES.Where(v => v.NUMSERIE == a.NUMSERIE).Where(v => v.NUMALBARAN == a.NUMALBARAN).DefaultIfEmpty()
                                     from a4 in db.ALBVENTAFIRMA.Where(v => v.SERIE == a.NUMSERIE).Where(v => v.NUMERO == a.NUMALBARAN).DefaultIfEmpty()
                                     from a5 in db.ARTICULOS.Where(v => v.CODARTICULO == a2.CODARTICULO).DefaultIfEmpty()
                                     from a6 in db.ICG_CONDUCTORES.Where(v => v.CODCONDUCTOR == a3.CHOFER).DefaultIfEmpty()
                                     from a7 in db.CLIENTES.Where(v => v.CODCLIENTE == a.CODCLIENTE).DefaultIfEmpty()
                                     from a8 in db.SERIESCAMPOSLIBRES.Where(v => v.SERIE == a3.NUMSERIE).DefaultIfEmpty()
                                     select new GuiaRemision
                                     {
                                         Establecimiento = a8.ESTABLECIMIENTO,
                                         Punto_Emision = a8.PUNTO_EMISION,
                                         NombreConductor = a6.NOMBRECONDUCTOR,
                                         IdConductor = a6.IDENTIFICACION,
                                         Placa_Alb = a3.PLACA_ALB,
                                         NumSerie = a.NUMSERIE,
                                         NumAlbaran = a.NUMALBARAN,
                                         Fecha = a.FECHA,
                                         Referencia = a2.REFERENCIA,
                                         Cantidad = a2.UNID1,
                                         Descripcion = a2.DESCRIPCION,
                                         Unidad = a5.UNID2C,
                                         Medida = a5.UNIDADMEDIDA,
                                         NombreCliente = a7.NOMBRECLIENTE,
                                         Direccion = a7.DIRECCION1,
                                         ClaveAcceso = a4.CLAVEACCESO
                                     }).OrderBy(x=>x.NombreCliente).ToList();

                    guiaRemisions = resultado;
                }
            }
            catch (Exception ex)
            {
                guiaRemisions = null;
                db?.Dispose();
            }
            return guiaRemisions;
        }
        public List<GuiaRemision> GetGuiaRemisions(string serie, int? numeros)
        {
            SCEntities db = null;
            List<GuiaRemision> guiaRemisions = new List<GuiaRemision>();
            try
            {
                using (db = new SCEntities())
                {
                    var resultado = (from a in db.ALBVENTACAB
                                     where a.NUMSERIE == serie && a.NUMALBARAN == numeros
                                     from a2 in db.ALBVENTALIN.Where(v => v.NUMSERIE == a.NUMSERIE).Where(v => v.NUMALBARAN == a.NUMALBARAN).DefaultIfEmpty()
                                     from a3 in db.ALBVENTACAMPOSLIBRES.Where(v => v.NUMSERIE == a.NUMSERIE).Where(v => v.NUMALBARAN == a.NUMALBARAN).DefaultIfEmpty()
                                     from a4 in db.ALBVENTAFIRMA.Where(v => v.SERIE == a.NUMSERIE).Where(v => v.NUMERO == a.NUMALBARAN).DefaultIfEmpty()
                                     from a5 in db.ARTICULOS.Where(v => v.CODARTICULO == a2.CODARTICULO).DefaultIfEmpty()
                                     from a6 in db.ICG_CONDUCTORES.Where(v => v.CODCONDUCTOR == a3.CHOFER).DefaultIfEmpty()
                                     from a7 in db.CLIENTES.Where(v => v.CODCLIENTE == a.CODCLIENTE).DefaultIfEmpty()
                                     from a8 in db.SERIESCAMPOSLIBRES.Where(v => v.SERIE == a3.NUMSERIE).DefaultIfEmpty()
                                     select new GuiaRemision
                                     {
                                         Establecimiento = a8.ESTABLECIMIENTO,
                                         Punto_Emision = a8.PUNTO_EMISION,
                                         NombreConductor = a6.NOMBRECONDUCTOR,
                                         IdConductor = a6.IDENTIFICACION,
                                         Placa_Alb = a3.PLACA_ALB,
                                         NumSerie = a.NUMSERIE,
                                         NumAlbaran = a.NUMALBARAN,
                                         Fecha = a.FECHA,
                                         Referencia = a2.REFERENCIA,
                                         Cantidad = a2.UNID1,
                                         Descripcion = a2.DESCRIPCION,
                                         Unidad = a5.UNID2C,
                                         Medida = a5.UNIDADMEDIDA,
                                         NombreCliente = a7.NOMBRECLIENTE,
                                         Direccion = a7.DIRECCION1,
                                         ClaveAcceso = a4.CLAVEACCESO
                                     }).OrderBy(x => x.NombreCliente).ToList();

                    guiaRemisions = resultado;
                }
            }
            catch (Exception ex)
            {
                guiaRemisions = null;
                db?.Dispose();
            }
            return guiaRemisions;
        }

    }
}
