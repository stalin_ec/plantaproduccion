﻿using PlantaProduccionSweet.Model;
using PlantaProduccionSweet.modelos;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlantaProduccionSweet.Controllers
{
    public class IncidenciasController
    {
        public ObservableCollection<INCIDENCIAS> GetIncidencias(DateTime FechaInicio, DateTime FechaFin, ref string Error)
        {
            ObservableCollection<INCIDENCIAS> incidencias = new ObservableCollection<INCIDENCIAS>();
            try
            {
                using(var db = new SCEntities())
                {
                    var a = db.INCIDENCIAS.Where(x => x.FECHA >= FechaInicio && x.FECHA <= FechaFin).ToList();
                    incidencias = new ObservableCollection<INCIDENCIAS>(a);
                    Error = "Se extrajeron : " + incidencias.Count() + " Registros. ";
                }
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                incidencias = null;
            }
            return incidencias;
        }

        public ObservableCollection<IncidenciaModel> GetIncidenciaModel(DateTime FechaInicio, DateTime FechaFin, ref string Error)
        {
            ObservableCollection<IncidenciaModel> incidencias = new ObservableCollection<IncidenciaModel>();
            try
            {
                using (var db = new SCEntities())
                {
                    var a = db.sis_pedidos(FechaInicio, FechaFin).ToList();
                    Error = "Se extrajeron : " + incidencias.Count() + " Registros. ";
                    foreach(var b in a)
                    {
                        incidencias.Add(new IncidenciaModel 
                        {
                            IsSelect = false,
                            Descripcion = b.DESCRIPCION,
                            EstadoCompra = b.ESTADOPEDCOMPRA,
                            Fecha = b.FECHA.GetValueOrDefault(),
                            NombreCliente = b.NOMBRECLIENTE,
                            NombreProveedor = b.NOMPROVEEDOR,
                            Supedido = b.CSUPEDIDO,
                            CSerie = b.CSERIE,
                            CNumero = b.CNUMERO.GetValueOrDefault(),
                            PSerie = b.PSERIE,
                            PNumero = b.PNUMERO.GetValueOrDefault()
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                incidencias = null;
            }
            return incidencias;
        }
    }
}
