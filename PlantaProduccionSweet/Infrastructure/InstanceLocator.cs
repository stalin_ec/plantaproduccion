﻿using PlantaProduccionSweet.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlantaProduccionSweet.Infrastructure
{
    public class InstanceLocator
    {
        public MainViewModel Main { get; set; }
        public ValidarViewModel Validar { get; set; }
        public ReportesViewModels Reporte { get; set; }

        public InstanceLocator()
        {
            Main = new MainViewModel();
            Validar = new ValidarViewModel();
            Reporte = new ReportesViewModels();
        }

    }
}
