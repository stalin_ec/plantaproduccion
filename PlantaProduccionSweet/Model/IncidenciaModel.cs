﻿using System;

namespace PlantaProduccionSweet.Model
{
    public class IncidenciaModel
    {
        public bool IsSelect { get; set; }
        public string Descripcion { get; set; }
        public string EstadoCompra { get; set; }
        public DateTime Fecha { get; set; }
        public string NombreCliente { get; set; }
        public string NombreProveedor { get; set; }
        public string Supedido { get; set; }
        public string CSerie { get; set; }
        public int CNumero { get; set; }
        public string PSerie { get; set; }
        public int PNumero { get; set; }
    }
}
