﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlantaProduccionSweet.Model.Matriz
{
    public class ArticuloPedidoCantidad
    {
        public string NumSerie { get; set; }
        public int NumPedido { get; set; }
        public string SerieAlbaran { get; set; }
        public int? NumAlbaran { get; set; }
        public DateTime? FechaEntrega { get; set; }
        public int CodCliente { get; set; }
        public string NombreCliente { get; set; }
        public string Alias { get; set; }
        public string Direccion { get; set; }
        public string Orden 
        { 
            get
            {
                return NumSerie + ' ' + NumPedido;
            }
        }
        public string Cabecera
        {
            get
            {
                return Alias +"\r\n" + Orden;
            }
        }
        public int? Tipo { get; set; }
        public int? CodArticulo { get; set; }
        public string Referencia { get; set; }
        public string Descripcion { get; set; }
        public double? Unidad { get; set; }
        public string Medida { get; set; }
        public double? Cantidad { get; set; }
        public double? Total { get; set; }
        public double? Stock { get; set; }
        public double? Diferencia { get; set; }

        public string TODORECIBIDO { get; set; }
    }
}
