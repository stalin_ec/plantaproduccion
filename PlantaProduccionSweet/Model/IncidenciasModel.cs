﻿using System;

namespace PlantaProduccionSweet.Model
{
    public class IncidenciasModel : BaseInpc
    {
        #region Atributos

        private bool isSelect;
        private DateTime fecha;
        private string serie;
        private int numero;

        #endregion

        #region Propiedades

        public int IdIncidencia { get; set; }

        public DateTime Fecha
        {
            get
            {
                return fecha;
            }
            set
            {
                if (fecha == value) return;
                fecha = value;
                OnPropertyChanged("Fecha");
            }
        }

        public string Serie
        {
            get
            {
                return serie;
            }
            set
            {
                if (serie == value) return;
                serie = value;
                OnPropertyChanged("Serie");
            }
        }

        public int Numero
        {
            get
            {
                return numero;
            }
            set
            {
                if (numero == value) return;
                numero = value;
                OnPropertyChanged("Numero");
            }
        }

        public bool IsSelect
        {
            get
            {
                return isSelect;
            }
            set
            {
                if (isSelect == value) return;
                isSelect = value;
                OnPropertyChanged("IsSelect");
            }
        }

        #endregion

        #region Metodos
        
        #endregion
    }
}
