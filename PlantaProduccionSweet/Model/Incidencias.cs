﻿using PlantaProduccionSweet.modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlantaProduccionSweet.Model
{
    public class Incidencias : BaseInpc
    {
        #region Atributos

        private static SCEntities _context;

        private DateTime fecha;
        private string serie;
        private int numero;

        #endregion

        #region Propiedades

        public int IdIncidencia { get; set; }

        public DateTime Fecha
        {
            get
            {
                return fecha;
            }
            set
            {
                if (fecha == value) return;
                fecha = value;
                OnPropertyChanged("Fecha");
            }
        }

        public string Serie
        {
            get
            {
                return serie;
            }
            set
            {
                if (serie == value) return;
                serie = value;
                OnPropertyChanged("Serie");
            }
        }

        public int Numero
        {
            get
            {
                return numero;
            }
            set
            {
                if (numero == value) return;
                numero = value;
                OnPropertyChanged("Numero");
            }
        }

        #endregion
    }
}
