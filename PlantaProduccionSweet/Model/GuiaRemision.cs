﻿
namespace PlantaProduccionSweet.Model
{
    using System;
    public class GuiaRemision : BaseInpc
    {
        public string Establecimiento { get; set; }
        public string Punto_Emision { get; set; }
        public string NombreConductor { get; set; }
        public string IdConductor { get; set; }
        public string Placa_Alb { get; set; }
        public string NumSerie { get; set; }
        public int NumAlbaran { get; set; }
        public DateTime? Fecha { get; set; }
        public string Referencia { get; set; }
        public double? Cantidad { get; set; }
        public string Descripcion { get; set; }
        public double? Unidad { get; set; }
        public string Medida { get; set; }
        public string NombreCliente { get; set; }
        public string Direccion { get; set; }
        public string ClaveAcceso { get; set; }
    }
}
