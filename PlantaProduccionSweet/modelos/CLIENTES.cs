//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PlantaProduccionSweet.modelos
{
    using System;
    using System.Collections.Generic;
    
    public partial class CLIENTES
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CLIENTES()
        {
            this.ALBVENTACAB = new HashSet<ALBVENTACAB>();
        }
    
        public int CODCLIENTE { get; set; }
        public string CODCONTABLE { get; set; }
        public string NOMBRECLIENTE { get; set; }
        public string NOMBRECOMERCIAL { get; set; }
        public string CIF { get; set; }
        public string ALIAS { get; set; }
        public string DIRECCION1 { get; set; }
        public string CODPOSTAL { get; set; }
        public string POBLACION { get; set; }
        public string PROVINCIA { get; set; }
        public string PAIS { get; set; }
        public string PERSONACONTACTO { get; set; }
        public string TELEFONO1 { get; set; }
        public string TELEFONO2 { get; set; }
        public string FAX { get; set; }
        public string FAXPEDIDOS { get; set; }
        public string TELEX { get; set; }
        public string E_MAIL { get; set; }
        public string CODCLISUYO { get; set; }
        public string NUMCUENTA { get; set; }
        public string CODBANCO { get; set; }
        public string NUMSUCURSAL { get; set; }
        public string DIGCONTROLBANCO { get; set; }
        public string CODPOSTALBANCO { get; set; }
        public string CODSWIFT { get; set; }
        public string NOMBREBANCO { get; set; }
        public string DIRECCIONBANCO { get; set; }
        public string POBLACIONBANCO { get; set; }
        public string ENVIOPOR { get; set; }
        public string ENVIODIRECION { get; set; }
        public string ENVIOCODPOSTAL { get; set; }
        public string ENVIOPOBLACION { get; set; }
        public string ENVIOPROVINCIA { get; set; }
        public string ENVIOPAIS { get; set; }
        public Nullable<double> CANTPORTESPAG { get; set; }
        public string TIPOPORTES { get; set; }
        public Nullable<int> NUMDIASENTREGA { get; set; }
        public Nullable<double> RIESGOCONCEDIDO { get; set; }
        public Nullable<short> TIPO { get; set; }
        public string RECARGO { get; set; }
        public string ZONA { get; set; }
        public Nullable<int> CODVENDEDOR { get; set; }
        public Nullable<short> DIAPAGO1 { get; set; }
        public Nullable<short> DIAPAGO2 { get; set; }
        public string OBSERVACIONES { get; set; }
        public string FACTURARSINIMPUESTOS { get; set; }
        public string APDOCORREOS { get; set; }
        public Nullable<double> DTOCOMERCIAL { get; set; }
        public Nullable<System.DateTime> FECHAMODIFICADO { get; set; }
        public string REGIMFACT { get; set; }
        public Nullable<int> CODMONEDA { get; set; }
        public string DIRECCION2 { get; set; }
        public string COMPRADOREDI { get; set; }
        public string RECEPTOREDI { get; set; }
        public string CLIENTEEDI { get; set; }
        public string PAGADOREDI { get; set; }
        public string USUARIO { get; set; }
        public string PASS { get; set; }
        public Nullable<int> TIPODOC { get; set; }
        public string NUMTARJETA { get; set; }
        public Nullable<System.DateTime> FECHANACIMIENTO { get; set; }
        public string SEXO { get; set; }
        public string NIF20 { get; set; }
        public string DESCATALOGADO { get; set; }
        public Nullable<int> TRANSPORTE { get; set; }
        public Nullable<int> MESVACACIONES { get; set; }
        public Nullable<int> GRUPOIMPRESION { get; set; }
        public Nullable<int> NUMCOPIASFACTURA { get; set; }
        public Nullable<int> TIPOCLIENTE { get; set; }
        public string CONDENTREGAEDI { get; set; }
        public string CONDENTREGA { get; set; }
        public Nullable<int> CODIDIOMA { get; set; }
        public string SERIE { get; set; }
        public string ALMACEN { get; set; }
        public string LOCAL_REMOTA { get; set; }
        public Nullable<int> EMPRESA { get; set; }
        public string CODENTREGA { get; set; }
        public string PROCEDENCIA { get; set; }
        public Nullable<int> CODIGOPROCEDENCIA { get; set; }
        public Nullable<int> IDSUCURSAL { get; set; }
        public Nullable<int> CODVISIBLE { get; set; }
        public string CODPAIS { get; set; }
        public Nullable<int> B2B_IDMAPPING { get; set; }
        public Nullable<int> FACTURARCONIMPUESTO { get; set; }
        public byte[] FOTOCLIENTE { get; set; }
        public Nullable<int> CARGOSFIJOSA { get; set; }
        public Nullable<int> TIPOTARJETA { get; set; }
        public string TARCADUCIDAD { get; set; }
        public string CVC { get; set; }
        public string CODCONTABLEDMN { get; set; }
        public Nullable<int> DISENYO_CAMPOSLIBRES { get; set; }
        public string MOBIL { get; set; }
        public Nullable<bool> NOCALCULARCARGO1ARTIC { get; set; }
        public Nullable<bool> NOCALCULARCARGO2ARTIC { get; set; }
        public Nullable<bool> ESCLIENTEDELGRUPO { get; set; }
        public string PASSWORDCOMMERCE { get; set; }
        public Nullable<int> TIPORESERVA { get; set; }
        public string REGIMRET { get; set; }
        public Nullable<int> TIPORET { get; set; }
        public Nullable<int> RET_TIPORETENCIONIVA { get; set; }
        public Nullable<double> RET_PORCEXCLUSION { get; set; }
        public Nullable<System.DateTime> RET_FECHAINIEXCLUSION { get; set; }
        public Nullable<System.DateTime> RET_FECHAFINEXCLUSION { get; set; }
        public Nullable<bool> CAMPOSLIBRESTOTALIZAR { get; set; }
        public Nullable<int> CODCLIASOC { get; set; }
        public Nullable<int> CARGOSEXTRASA { get; set; }
        public Nullable<double> COMISION { get; set; }
        public Nullable<int> PROVEEDORCOMISION { get; set; }
        public Nullable<bool> COMISIONESFACTURABLES { get; set; }
        public Nullable<bool> LOCALIZADOROBLIGATORIO { get; set; }
        public bool RECC { get; set; }
        public string BLOQUEADO { get; set; }
        public string ORDENADEUDO { get; set; }
        public Nullable<int> SUBNORMA { get; set; }
        public Nullable<int> SECUENCIAADEUDO { get; set; }
        public string CODIGOIBAN { get; set; }
        public Nullable<System.DateTime> FECHAFIRMAORDENADEUDO { get; set; }
        public byte[] VERSION { get; set; }
        public Nullable<int> TIPODOCIDENT { get; set; }
        public string PERSONAJURIDICA { get; set; }
        public bool RECIBIRINFORMACION { get; set; }
        public Nullable<bool> CONFIRMACIONRESERVACOMPLETA { get; set; }
        public Nullable<int> CONFIRMACIONRESERVADISENYO { get; set; }
        public Nullable<int> CONFIRMACIONRESERVAPLANTILLACOMPLETA { get; set; }
        public Nullable<int> CONFIRMACIONRESERVAPLANTILLASIMPLE { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALBVENTACAB> ALBVENTACAB { get; set; }
    }
}
