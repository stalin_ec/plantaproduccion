//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PlantaProduccionSweet.modelos
{
    using System;
    using System.Collections.Generic;
    
    public partial class ARTICULOS
    {
        public int CODARTICULO { get; set; }
        public string DESCRIPCION { get; set; }
        public string DESCRIPADIC { get; set; }
        public Nullable<int> TIPOIMPUESTO { get; set; }
        public Nullable<short> DPTO { get; set; }
        public Nullable<short> SECCION { get; set; }
        public Nullable<short> FAMILIA { get; set; }
        public Nullable<short> SUBFAMILIA { get; set; }
        public Nullable<short> LINEA { get; set; }
        public string TEMPORADA { get; set; }
        public string GENERARETIQ { get; set; }
        public byte[] FOTO { get; set; }
        public Nullable<int> MARCA { get; set; }
        public string CODTALLA { get; set; }
        public string NORMA { get; set; }
        public string TACON { get; set; }
        public string COMPOSICION { get; set; }
        public string ARTICULOVIRTUAL { get; set; }
        public string TIENETC { get; set; }
        public Nullable<double> UNID1C { get; set; }
        public Nullable<double> UNID2C { get; set; }
        public Nullable<double> UNID3C { get; set; }
        public Nullable<double> UNID4C { get; set; }
        public Nullable<double> UNID1V { get; set; }
        public Nullable<double> UNID2V { get; set; }
        public Nullable<double> UNID3V { get; set; }
        public Nullable<double> UNID4V { get; set; }
        public string ESKIT { get; set; }
        public string USARNUMSERIE { get; set; }
        public string GENNUMSERIE { get; set; }
        public Nullable<int> TIPO { get; set; }
        public Nullable<System.DateTime> FECHAMODIFICADO { get; set; }
        public string REFPROVEEDOR { get; set; }
        public string CONTRAPARTIDAVENTA { get; set; }
        public string CONTRAPARTIDACOMPRA { get; set; }
        public string UNIDADMEDIDA { get; set; }
        public Nullable<double> UDSELABORACION { get; set; }
        public Nullable<double> MEDIDAREFERENCIA { get; set; }
        public string PORPESO { get; set; }
        public string USASTOCKS { get; set; }
        public Nullable<int> IMPUESTOCOMPRA { get; set; }
        public string DESCATALOGADO { get; set; }
        public Nullable<double> UDSTRASPASO { get; set; }
        public string TIPOARTICULO { get; set; }
        public string GARANTIACOMPRA { get; set; }
        public string GARANTIAVENTA { get; set; }
        public Nullable<int> COLORFONDO { get; set; }
        public Nullable<int> COLORTEXTO { get; set; }
        public string TIPOSAT { get; set; }
        public Nullable<System.DateTime> FACTPORHORA { get; set; }
        public Nullable<int> CONSUMADIC { get; set; }
        public Nullable<double> MARGEN { get; set; }
        public Nullable<double> CARGO1 { get; set; }
        public Nullable<double> CARGO2 { get; set; }
        public Nullable<int> NUMCONSUMICIONES { get; set; }
        public Nullable<int> CODCENTRAL { get; set; }
        public string CONTRAPARTIDACOSTEVENTAS { get; set; }
        public Nullable<int> CODDISENY { get; set; }
        public Nullable<int> CODIGOADUANA { get; set; }
        public string MEDIDA2 { get; set; }
        public string VISIBLEWEB { get; set; }
        public Nullable<int> DIASCADUCIDAD { get; set; }
        public Nullable<double> PORCRETENCION { get; set; }
        public string CONTRAPARTIDACONSUMO { get; set; }
        public string CONTRAPARTIDAVENTADMN { get; set; }
        public string CONTRAPARTIDACOMPRADMN { get; set; }
        public string CONTRAPARTIDACOSTEVENTASDMN { get; set; }
        public Nullable<bool> DESCARGADO { get; set; }
        public Nullable<double> PRECIOMINIMO { get; set; }
        public Nullable<double> PRECIOMAXIMO { get; set; }
        public string PRECIOLIBRE { get; set; }
        public Nullable<bool> HIOPOS_IMPRIMIRCOCINA { get; set; }
        public Nullable<bool> HIOPOS_EBT { get; set; }
        public Nullable<int> HIOPOS_TAKEAWAY { get; set; }
        public string AVISOVENTA { get; set; }
        public byte[] FOTOSHA { get; set; }
        public Nullable<bool> FORZARUDSENTERASVENTA { get; set; }
        public Nullable<int> DURACION { get; set; }
        public Nullable<int> IDTALONARIO { get; set; }
        public Nullable<bool> HIOPOS_IMPRIMIRCOCINA2 { get; set; }
        public Nullable<bool> HIOPOS_IMPRIMIRCOCINA3 { get; set; }
        public Nullable<bool> HIOPOS_ISMODIFICADOR { get; set; }
        public Nullable<bool> NODTOAPLICABLE { get; set; }
        public string CONTRAPARTIDADEVOLCOMPRA { get; set; }
        public string CONTRAPARTIDADEVOLVENTA { get; set; }
        public string CONTRAPARTIDADEVOLCOSTEVENTA { get; set; }
        public string CONTRAPARTIDADEVOLCOMPRADMN { get; set; }
        public string CONTRAPARTIDADEVOLVENTADMN { get; set; }
        public string CONTRAPARTIDADEVOLCOSTEVENTASDM { get; set; }
        public Nullable<int> REGIMRET_IVA { get; set; }
        public Nullable<int> REGIMRET_BASEIMPONIBLE { get; set; }
        public string SOLICITARCOMENTARIO { get; set; }
        public string DIRCONTAB { get; set; }
        public Nullable<int> SUBEMPRESA { get; set; }
        public string CONTRAPARTIDAVENTAEXONERADA { get; set; }
        public string CONTRAPARTIDADEVOLVENTAEXONERADA { get; set; }
        public string CONTRAPARTIDAFALTANTESINVENTARIO { get; set; }
        public string CONTRAPARTIDASOBRANTESINVENTARIO { get; set; }
        public string CONTRAPARTIDAORDENESFAB { get; set; }
        public byte[] VERSION { get; set; }
        public string FIJARPV_CADADIA { get; set; }
        public string FIJARPV_ALCAMBIARPRECIO { get; set; }
        public Nullable<int> FORMADEPESO { get; set; }
        public string CONTRAPARTIDACONSUMOS { get; set; }
        public string USARFID { get; set; }
    }
}
